FROM python:3.10.1

COPY . /app
WORKDIR /app

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

EXPOSE 8000

CMD ["uvicorn", "gym:app", "--reload", "--host", "0.0.0.0", "--port", "8000"]
