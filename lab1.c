#include <stdio.h>
#include <math.h>

int main()
{
    double x;
    printf("Enter x { -1.0 <= x <= 1.0 } -> ");
    scanf("%lf", &x);
    if (-1.0 <= x && x <= 1.0)
    {
        double y = sqrt(sin(2.0*x)) + sqrt(sin(2.0*x));
        double z = sqrt(x, 4.0);
        printf("y(x) = %lf\nz(y) = %lf\n", y, z);
    }
    else
        printf("x value is incorrect!\n");
    return 0;
}


