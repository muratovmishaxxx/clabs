#include <stdio.h>
#include <math.h>
#define _USE_MATH_DEFINES
int main()
{
double x;
printf("Enter x { 0.0 <= x <= Pi/2 } -> ");
scanf("%lf", &x);
if (0.0 <= x && x <= M_PI/2)
{
double y = sqrt(sin (2.0 * x))+sqrt(sin (3.0 * x));
double z = sqrt(sqrt(log(tan(y - M_PI/8))));
printf("y(x) = %lf\nz(y) = %lf\n", y, z);
}
else
printf("x value is incorrect!\n");
return 0;
}
