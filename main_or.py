from http.client import HTTPException
from typing import Optional, List, Any

import uvicorn
from fastapi import FastAPI, Depends
from pydantic import BaseModel, Field
from sqlalchemy import create_engine, Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from sqlmodel import Session

SQLALCHEMY_DATABASE_URL = "sqlite:///./gym.db"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

class UserDB(Base):
    __tablename__ = "User"
    id = Column(Integer, primary_key=True, index=True)
    user_name = Column(String)
    user_password = Column(String)
    user_token = Column(String)
    user_gender = Column(String)
    phone_number = Column(Integer)  
    user_mail = Column(String)
    timedelta = Column(Integer)
    admin = relationship("AdminDB", back_populates="user")

class AdminDB(Base):
    __tablename__ = "Admin"
    id = Column(Integer, primary_key=True, index=True)
    admin_name = Column(String)
    admin_password = Column(String)
    admin_token = Column(String)
    user = relationship("UserDB", back_populates="admin")

app = FastAPI()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

class User(BaseModel):
    user_name: str
    user_password: str
    user_token: str
    user_gender: str
    phone_number: int  
    user_mail: str
    timedelta: int

    class Config:
        orm_mode = True

user_data = {
    "user_name": "nickname",
    "user_password": "min length 8 with symbols numbers and characters",
    "user_token": "token",
    "user_gender": "gender",
    "phone_number": "79045557777",
    "user_mail": "mail",
    "timedelta": "14"
}
user = User(**user_data)
print(user)

class Admin(BaseModel):
    admin_name: str
    admin_password: str
    admin_token: str
    class Config:
        orm_mode = True

admin_data = {
    "admin_name": "first and last name",
    "admin_password": "password",
    "admin_token": "token"
}
admin = Admin(**admin_data)
print(admin)

@app.get("/")
async def read_root():
    return {"Hello": "World"}

@app.get("/user/{id}")
def update_user(id:int, user: User):
    return {"user_id": id, "user_name": user}

@app.put("/admin/{admin_id}")
async def update_admin(admin_id: int, admin: Admin):
    return {"admin_id": admin_id, "admin_name": admin}

@app.get("/admin/{admin_id}", response_model=Admin)
async def read_admin(admin_id: int, db: Session = Depends(get_db)):
    db_admin = db.query(AdminDB).filter(AdminDB.id == admin_id).first()
    if db_admin is None:
        raise HTTPException(status_code=404, detail="Item not found")
    return db_admin

@app.get("/user/{user_id}", response_model=User)
def read_user(user_id: int, db: Session = Depends(get_db)):
    db_user = db.query(UserDB).filter(UserDB.id == user_id).first()
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user

#CRUD

@app.post("/api/doc")
def create_admin(data  = Body(), db: Session = Depends(get_db)):
    admin = AdminDB(id=data["id"], admin_name=data["name"], admin_password=data["password"], admin_token=data["token"])
    db.add(admin)
    db.commit()
    db.refresh(admin)
    return admin

@app.get("/api/doc")
def get_admin(db: Session = Depends(get_db)):
    return db.query(AdminDB).all()

@app.get("/api/doc/{id}")
def get_admin(id, db: Session = Depends(get_db)):  
    admin = db.query(AdminDB).filter(AdminDB.id == id).first()
    if admin==None:  
        return JSONResponse(status_code=404, content={ "message": "Admin not found"})
    return admin

@app.put("/api/doc")
def edit_admin(data  = Body(), db: Session = Depends(get_db)):
    admin = db.query(AdminDB).filter(AdminDB.id == data["id"]).first()
    if admin == None: 
        return JSONResponse(status_code=404, content={ "message": "Admin not found"})
    admin.id = data["id"]
    admin.admin_name=data["name"]
    admin.admin_password=data["password"]
    admin.admin_token=data["token"]
    db.commit()
    db.refresh(admin)
    return admin

@app.delete("/api/users/{id}")
def delete_admin(id, db: Session = Depends(get_db)):
    admin = db.query(AdminDB).filter(AdminDB.id == id).first()
    if admin == None:
        return JSONResponse( status_code=404, content={ "message": "Admin not found"})
    db.delete(AdminDB) 
    db.commit()
    return admin

import asyncio
import httpx

async def fetch(client):
    response = await client.get('https://api-ninjas.com/api/randomword')
    return response.json()
async def main():
    async with httpx.AsyncClient() as client:
        results = await asyncio.gather(*[fetch(client) for _ in range(100)])
        return results

@app.get("/api/word")
async def get_word():
    return await main()


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
