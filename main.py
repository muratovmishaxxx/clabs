from typing import Union

import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel, Field

app = FastAPI()

class User(BaseModel):
    user_name: str
    user_password: str
    user_token: str
    user_gender: str
    phone_number: int
    user_mail: str
    timedelta: int

user_data = {
    "user_name": "nickname",
    "user_password": "min length 8 with symbols numbers and characters",
    "user_token": "token",
    "user_gender": "gender",
    "phone_number": "79045557777",
    "user_mail": "mail",
    "timedelta": "14"
}
user = User(**user_data)
print(user)

class Admin(BaseModel):
    admin_name: str
    admin_password: str
    admin_token: str 

admin_data = {
    "admin_name": "first and last name",
    "admin_password": "password",
    "admin_token": "token"
}
admin = Admin(**admin_data)
print(admin)

@app.get("/")
async def read_root():
    return {"Hello": "World"}

@app.put("/user/{user_id}")
async def update_user(user_id: int, user: User):
    return {"user_id": user_id, "user_name": user}

@app.put("/admin/{admin_id}")
async def update_admin(admin_id: int, admin: Admin):
    return {"admin_id": admin_id, "admin_name": admin}

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)